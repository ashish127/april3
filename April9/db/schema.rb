# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180409122327) do

  create_table "articles", force: :cascade do |t|
    t.string "title"
    t.text "text"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "pin"
  end

  create_table "bar", force: :cascade do |t|
    t.string "proname"
    t.string "Info"
    t.integer "Quantitiy"
    t.integer "article_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.decimal "price", precision: 5, scale: 2
    t.string "supplier_type"
    t.integer "supplier_id"
    t.index ["article_id"], name: "index_bar_on_article_id"
    t.index ["supplier_type", "supplier_id"], name: "index_bar_on_supplier_type_and_supplier_id"
  end

  create_table "categorization", id: false, force: :cascade do |t|
    t.integer "article_id", null: false
    t.integer "product_id", null: false
  end

  create_table "thirds", force: :cascade do |t|
    t.string "product_id"
    t.string "category_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
