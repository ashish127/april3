class CreateThirds < ActiveRecord::Migration[5.1]
  def change
    create_table :thirds do |t|
      t.string :product_id
      t.string :category_id

      t.timestamps
    end
  end
end
