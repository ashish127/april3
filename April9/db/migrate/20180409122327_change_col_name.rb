class ChangeColName < ActiveRecord::Migration[5.1]
 def self.up
    rename_table :products, :bar
  end

  def self.down
    rename_table :bar, :products
  end

end
