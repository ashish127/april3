class CreateJoinTableArticleProducts < ActiveRecord::Migration[5.1]
  def change
    create_join_table :articles, :products, table_name: :categorization do |t|
      # t.index [:article_id, :product_id]
      # t.index [:product_id, :article_id]
    end
  end
end
