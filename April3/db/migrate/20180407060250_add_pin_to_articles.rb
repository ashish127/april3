class AddPinToArticles < ActiveRecord::Migration[5.1]
  def change
    add_column :articles, :pin, :string
  end
end
