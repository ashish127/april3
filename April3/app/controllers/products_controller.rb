class ProductsController < ApplicationController

# def create
	
# 	render plain: params[:product_params].inspect
# 	 end
def create
    @article = Article.find(params[:article_id])
    @product = @article.products.create(product_params)
    redirect_to article_path(@article)
  end
 
  private
    def product_params
      params.require(:product).permit(:Name, :Info, :Quantitiy)
    end
end
